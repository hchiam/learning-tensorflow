# Learning TensorFlow

Just one of the things I'm learning. <https://github.com/hchiam/learning>

## [TensorFlow.js](https://github.com/hchiam/learning-tensorflow/tree/master/js)

## [TensorFlow Lite](https://github.com/hchiam/learning-tensorflow/tree/master/lite)

## Quickly leverage existing models

<https://tfhub.dev>

They can also come with example use code, like this example with code ready to copy-paste: <https://tfhub.dev/google/aiy/vision/classifier/birds_V1/1>

## Other Repos for Further Reference

<https://github.com/hchiam/tensorflow.js_explained>

<https://github.com/hchiam/TensorFlow-in-a-Nutshell>
